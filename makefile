SHELL = /bin/bash

dev-start:;
	mvn compile -e quarkus:dev

clean:;
	mvn clean

compile-java:;
	mvn compile package

compile-java-quick:;
	mvn compile package -Dmaven.test.skip=true

compile: clean compile-java;

compile-quick: clean compile-java-quick;

compile-start: compile-quick;
	docker compose down
	docker compose build
	docker compose up -d